<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idClient')->unsigned()->index('idClient_foreign_2')->comment('ID do cliente');
            $table->enum('type', array('Deposito','Saque'))->nullable()->comment('Tipo de transacao');
            $table->double('value')->default(0.00);
            $table->timestamps();
            $table->foreign('idClient', 'users_idClient_fk2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trasations');
    }
}
