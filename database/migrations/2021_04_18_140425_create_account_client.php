<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_client', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idClient')->unsigned()->index('idClient_foreign')->comment('ID do cliente');
            $table->double('balance')->default(0.00);
            $table->timestamps();
            $table->foreign('idClient', 'users_idClient_fk1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_client');
    }
}
