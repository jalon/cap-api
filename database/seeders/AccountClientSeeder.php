<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account_client')->insert([
            'idClient' => 1,
            'balance' => 10.00,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
