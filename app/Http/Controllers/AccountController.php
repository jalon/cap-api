<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Transations;
use App\Models\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    protected $account;

    public function __construct(User $user, Account $account, Transations $transations)
    {
        $this->account = $account;
        $this->account->setTable('account_client');

        $this->transations = $transations;
        $this->transations->setTable('transations');

        $this->user = $user;
        $this->user->setTable('users');
    }

    public function show($id)
    {
        $balance = $this->account->where('idClient', '=', $id)->first();

        return response()->json($balance);
    }

    public function user($id)
    {
        $user = $this->user->where('id', '=', $id)->first();

        return response()->json($user);
    }

    public function transations(Request $request)
    {
        $data = [
            'idClient' => $request->idClient,
            'type' => $request->type,
            'value' => $request->value
        ];

        $account = $this->account->where('idClient', '=', $request->idClient)->first();

        if($request->type == 'Deposito'){
            $balance = (double)$account['balance'] + (double)$request->value;
            $account->update(["balance" => $balance]);
        } else {
            $balance = (double)$account['balance'] - (double)$request->value;
            $account->update(["balance" => $balance]);
        }

        $this->transations->create($data);

        return response()->json(['status' => 1, 'response' => $request->type.' realizado com sucesso!']);
    }
}
