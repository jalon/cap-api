<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transations extends Model
{
    protected $guarded = ['deleted_at', 'created_at', 'updated_at'];
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];
    protected $fillable = ['idClient','type','value'];

}
