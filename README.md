#API REST Capgemini

O Banco Capgemini necessita criar uma Web API Rest para expor os seguintes serviços para
seus clientes:
1) Serviço para verificar o saldo de uma conta corrente;
2) Serviço para realizar um depósito em uma determinada conta corrente;
3) Serviço para realizar um saque de uma determinada conta corrente.

## Quick start

- Run `composer install`
- Run `php artisan migrate`
- Run `php artisan db:seed UsersTableSeeder`
- Run `php artisan db:seed AccountClientSeeder`
- Run `php artisan serve`
